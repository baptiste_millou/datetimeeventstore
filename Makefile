BIN=./env/bin/

init:
	python3 -m venv ./env
	$(BIN)pip install -r requirements.txt
	mongod --config /usr/local/etc/mongod.conf --fork

test:
	$(BIN)python test.py

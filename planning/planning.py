__all__ = ['DatetimeEventStore', 'DateUsedError', 'PastDateError']

import datetime

import pymongo


class DateUsedError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Cannot save event at {} as the date is already in use.'.format(self.value.isoformat())


class PastDateError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return 'Cannot save event at {} as the date is in the past.'.format(self.value.isoformat())


class DatetimeEventStore():
    """Store and retrieve Events from database."""

    def __init__(self, db=None, collection='planning', tz=datetime.timezone.utc):
        self.db = db if db is not None else pymongo.MongoClient(tz_aware=True).ikare
        self.col = collection
        self.tz = tz

    # def __del__(self):
    #     self.db.close()

    def store_event(self, at, event):
        if at < datetime.datetime.now(tz=self.tz):
            # Event is in the past
            raise PastDateError(at)
        # Check that no event exists at the same datetime.
        exists = self.db[self.col].find_one({'date': at})
        if exists:
            raise DateUsedError(at)
        # Save event
        self.db[self.col].insert_one({'name': event, 'date': at})

    def get_events(self, start, end):
        events = self.db[self.col].find({ 'date': {'$lt': end, '$gt': start}})
        return tuple(events)

from setuptools import setup, find_packages


setup(
    name='planning',
    version='0.1.1',
    packages=find_packages(),
    description='Event manager developped as a technical test.',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    author='Baptiste Millou',
    author_email='baptiste.millou@gmail.com',
    url='https://bitbucket.org/baptiste_millou/datetimeeventstore',
    license='GPL-3.0-only',
    # license_file='LICENSE',
    install_requires=['tinydb', ],
    test_suite='test.TestPlanningMethods',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License version 3',
        'Programming Language :: Python',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    python_requires='>=3.6',
)

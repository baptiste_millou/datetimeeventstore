# DatetimeEventStore

Gestionnaire d'évènements développé dans le cadre d'un test technique.

## Prérequis

    1. Python 3
    2. Git
    3. mongodb

## Installation

`git clone https://baptiste_millou@bitbucket.org/baptiste_millou/datetimeeventstore.git planning && cd planning`
`make init`
`make test`

## Usage

Activez l'environnement virtuel python avec `source env/bin/activate`, puis:

```python
import datetime
import random

from planning.planning import DatetimeEventStore

store = DatetimeEventStore()

# Generate a bunch of events to be stored in a period of 20 years.
start_ts = datetime.datetime(2030, 1, 1).timestamp()
end_ts = datetime.datetime(2050, 1, 1).timestamp()

for i in range(1000):
    dt = datetime.datetime.fromtimestamp(
        random.randint(start_ts, end_ts)
    )
    store.store_event(at=dt, event="Event number %d." % i)

for event in store.get_events(
    start=datetime.datetime(year=2038, month=1, day=1),
    end=datetime.datetime(year=2038, month=2, day=1)
    ):
    print(event)

```

## Choix techniques

L'objectif étant de réaliser un package python ayant vocation à être utilisé et maintenu par d'autres développeurs, j'ai essayé de rester le plus agnostique possible dans mes choix technologiques, notamment:

- Avoir le moins de dépendances possibles
- Utiliser la version actuelle de python

Le package ayant besoin de stocker des données, j'ai quand même utilisé une base de données minimaliste (fichier json, via le package tinyDB), mais à ce stade du développement, utiliser un autre système de stockage devrait être assez simple, et le choix de ce système fait partie des informations que je m'attends à avoir lors du développement d'un projet de ce genre; et dans le cas contraire, le choix de ce système est probablement secondaire et j'utiliserai sûrement quelque chose de similaire.
Version 0.1.1: J'ai changé la base de données pour passer sur du mongoDB, afin de permettre une plus grande vitesse de lecture/écriture.

En termes de fonctionnalités, je me suis limité au minimum des fonctionnalités demandée. N'ayant pas plus d'information sur le contexte dans lequel cette fonctionnalité sera utilisée, tout développement supplémentaire de ma part aurait de grandes chances d'être inutilisé. J'ai tout de même empêché l'enregistrement d'un' évènements dans le passé, ainsi que deux évènements au même instant. Cela me paraît être important dans une application manipulant des évènements, et si ça s'avère être inutile ce sont des fonctions très simples à modifier.

J'ai suivi la méthode TDD pour écrire ce programme, même si je n'ai pas vraiment eut l'opportunité de l'utiliser par le passé, je pense que c'est une méthode solide, et particulièrement adaptée à un petit projet de ce type où la totalité des fonctionnalités attendues est connue avant d'attaquer le développement, et ne changera pas en cours de route, ce qui n'est pas toujours le cas.

J'ai aussi choisi d'associer ce projet à une license open-source, mais dans le cas d'un développement interne à un projet commercial, la license serait probablement différente. Je n'ai aussi pas mis ce projet en ligne sur PyPI, ce qui serait l'étape suivante logique pour un projet open-source, car il n'a pas vraiment d'utilité en dehors de l'exercice technique.
Je n'ai aussi pas ajouté de documentation en dehors de quelques commentaires et de ce Readme, car la source est extrêmement succincte et, je l'espère, suffisamment évidente pour qu'un développeur puisse s'approprier le projet.

J'ai passé une dizaine d'heures sur ce projet, dont une grosse moitié dédiée à faire fonctionner l'architecture imposée pour un package python, ainsi que la réalisation des *annexes* qui devraient grandement faciliter l'installation et la réutilisation de ce projet.

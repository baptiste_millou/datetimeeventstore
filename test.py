import datetime
import os
import unittest

from planning.planning import DatetimeEventStore, DateUsedError, PastDateError
import pymongo


class TestPlanningMethods(unittest.TestCase):

    def setUp(self):
        self.db = pymongo.MongoClient(tz_aware=True).test
        self.planner = DatetimeEventStore(self.db, 'planning')

        self.date1 = datetime.datetime(year=3019, month=5, day=2, tzinfo=self.planner.tz)
        self.date2 = datetime.datetime(year=3020, month=8, day=3, tzinfo=self.planner.tz)
        self.date3 = datetime.datetime(year=3021, month=11, day=4, tzinfo=self.planner.tz)
        events = [
            {'name': 'test event 1', 'date': self.date1},
            {'name': 'test event 2', 'date': self.date2},
            {'name': 'test event 3', 'date': self.date3}
        ]
        self.db['planning'].insert_many(events)

    def tearDown(self):
        # Delete db
        self.db['planning'].drop()
        del self.planner, self.db

    def test_get_events(self):
        events = self.planner.get_events(
            start=datetime.datetime(year=3020, month=1, day=1, tzinfo=self.planner.tz),
            end=datetime.datetime(year=3020, month=12, day=31, tzinfo=self.planner.tz))

        self.assertEqual(len(events), 1, msg="There should only be one and only one event between start and end.")
        self.assertEqual(events[0]['name'], 'test event 2')
        self.assertEqual(events[0]['date'], self.date2)

    def test_get_no_events(self):
        events = self.planner.get_events(
            start=datetime.datetime(year=2020, month=1, day=1, tzinfo=self.planner.tz),
            end=datetime.datetime(year=2020, month=12, day=3, tzinfo=self.planner.tz))
        self.assertEqual(len(events), 0, msg="There should not be any event between start and end.")

    def test_store_event_duplicate(self):
        with self.assertRaises(DateUsedError, msg="It shouldn't be possible to have 2 events at the same time."):
            self.planner.store_event(self.date1, 'test event 1')

    def test_store_event_past(self):
        past_event = {'date': datetime.datetime(year=2002, month=10, day=1, tzinfo=self.planner.tz), 'name': 'past event'}
        with self.assertRaises(PastDateError, msg="It shouldn't be possible to have an event in the past."):
            self.planner.store_event(past_event['date'], past_event['name'])

    def test_store_event_future(self):
        future_date = datetime.datetime(year=3022, month=10, day=1, tzinfo=self.planner.tz)
        future_name = 'future event'

        exists = self.db['planning'].find_one({'date': future_date})
        self.assertIsNone(exists)

        self.planner.store_event(future_date, future_name)

        event = self.db['planning'].find_one({'name': future_name})
        self.assertTrue(event, msg="DatetimeEventStore.store_event() should have created an event.")
        self.assertEqual(event['date'], future_date,
            msg="It should be possible to have an event at a future date, when no other event exists.")

    def test_tz_aware_dates(self):
        event = self.db['planning'].find_one({})
        self.assertIsNotNone(event['date'].tzinfo, msg="Dates retrieved should be aware.")
        self.assertIsNotNone(event['date'].tzinfo.utcoffset(event['date']), msg="Dates retrieved should be aware.")


if __name__ == '__main__':
    unittest.main()
